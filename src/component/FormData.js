import React, { useState, useMemo, useEffect } from "react";
import Select from "react-select";
import countryList from "react-select-country-list";
import { useSelector, useDispatch } from "react-redux";
import { CREATE } from "../redux/ActionType";
import { createRecord, updateRecord, selectRecord } from "../redux/Actions";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();
function FormData({
  modalStatus,
  setModalStatus,
  setUserDetails,
  userDetails,
  setID,
  newId,
  editStatus,
  setEditStatus,
  setIsChecked,
  isChecked,
  // toast,
}) {
  const dispatch = useDispatch();
  const tableDetalis = useSelector((state) => state.details);
  const options = useMemo(() => countryList().getData(), []);
  const [nameStatus, setNameStatus] = useState(false);
  const [countryStatus, setCountryStatus] = useState(false);
  const [emailStatus, setEmailStatus] = useState(false);
  const [ageStatus, setAgeStatus] = useState(false);
  const [dobStatus, setDobStatus] = useState(false);
  const [cityStatus, setCityStatus] = useState(false);
  const [zipStatus, setZipStatus] = useState(false);
  const [addressStatus, setAddressStatus] = useState(false);
  const [genderStatus, setGenderStatus] = useState(false);

  const changeHandler = (value) => {
    setUserDetails({
      ...userDetails,
      country: value,
      
    });
  };

  const handleError = () => {
    let isSubmit = false;
    if (userDetails.name === undefined || userDetails.name === "") {
      setNameStatus(true);
      isSubmit = true;
    } else {
      setNameStatus(false);
      // isSubmit = false;
    }
    if (userDetails.email === undefined || userDetails.email === "") {
      setEmailStatus(true);
      isSubmit = true;
    } else {
      setEmailStatus(false);
      // isSubmit = false;
    }
    if (userDetails.age === undefined || userDetails.age === "") {
      setAgeStatus(true);
      isSubmit = true;
    } else {
      setAgeStatus(false);
      // isSubmit = false;
    }
    if (userDetails.date === undefined || userDetails.date === "") {
      setDobStatus(true);
      isSubmit = true;
    } else {
      setDobStatus(false);
      // isSubmit = false;
    }
    if (userDetails.address === undefined || userDetails.address === "") {
      setAddressStatus(true);
      isSubmit = true;
    } else {
      setAddressStatus(false);
      // isSubmit = false;
    }
    if (userDetails.city === undefined || userDetails.city === "") {
      setCityStatus(true);
      isSubmit = true;
    } else {
      setCityStatus(false);
      // isSubmit = false;
    }
    if (userDetails.city === undefined || userDetails.city === "") {
      setCityStatus(true);
      isSubmit = true;
    } else {
      setCityStatus(false);
      // isSubmit = false;
    }
    if (userDetails.gender === undefined || userDetails.gender === "") {
      setGenderStatus(true);
      isSubmit = true;
    } else {
      setGenderStatus(false);
      // isSubmit = false;
    }
    if (userDetails.country === undefined || userDetails.country === "") {
      setCountryStatus(true);
      isSubmit = true;
    } else {
      setCountryStatus(false);
      // isSubmit = false;
    }
    if (userDetails.zip === undefined || userDetails.zip === "") {
      setZipStatus(true);
      isSubmit = true;
    } else {
      setZipStatus(false);
      // isSubmit = false;
    }
    return isSubmit;
  };

  const onSubmit = () => {
    if (isChecked) {
      if (!handleError()) {
        dispatch(createRecord(userDetails));
        setModalStatus(false);
        setID([]);
        setUserDetails({
          isSelected: false,
        });
        setEditStatus(false);
        setIsChecked(false);
        // toast("Record added successfully");
        toast.success("Record added successfully", {
          autoClose: 2000,
          position: toast.POSITION.BOTTOM_CENTER,
        });
      }
    }
  };

  const handleClear = () => {
    setUserDetails({
      isSelected: false,
      name: "",
      email: "",
      age: "",
      date: "",
      gender: "",
      address: "",
      city: "",
      country: "",
      zip: "",
    });
    setIsChecked(false);
    setAddressStatus(false);
    setCityStatus(false);
    setNameStatus(false);
    setEmailStatus(false);
    setCountryStatus(false);
    setZipStatus(false);
    setCityStatus(false);
    setDobStatus(false);
    setAgeStatus(false);
    setGenderStatus(false);
  };

  const onUpdate = () => {
    if (isChecked) {
      if (!handleError()) {
    dispatch(updateRecord(userDetails, newId[0]));
    setUserDetails({
      isSelected: false,
    });
    setEditStatus(false);
    setModalStatus(false);
    setID([]);
    toast.success("Record updated successfully", {
      autoClose: 2000,
      position: toast.POSITION.BOTTOM_CENTER,
    });
  }
}
  };
  return (
    <div >
      {modalStatus && (
        <div
          style={{
            width: "100%",
            height:"100%",
            // marginLeft: "15%",
            // marginTop: "1%",
            zIndex: 3,
            // position: "fixed",
            background: "grey",
            // opacity: 0.4,
          }}
        >
          <div
            class="card shadow p-3 mb-5 bg-white rounded"
            style={{
              width: "70%",
              marginLeft: "15%",
              // marginBottom: "0%",
              // marginTop: "2%",
              
              //   zIndex: 3,
              //   position: "fixed",
              // opacity: 1,
            }}
          >
            <div class="card-header d-flex">
              <h4 class="text-left">Personal_Details</h4>
              <i
                class="fas fa-times "
                style={{
                  marginLeft: "75%",
                  marginTop: "2%",
                }}
                onClick={() => {
                  setModalStatus(false);
                  setEditStatus(false);
                }}
              ></i>
            </div>
            <div class="card-body text-left">
              <form>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="inputName">Name *</label>
                    <input
                      type="text"
                      class="form-control"
                      id="inputName"
                      placeholder="Name"
                      value={userDetails.name}
                      onChange={(e) =>
                        setUserDetails({
                          ...userDetails,
                          name: e.target.value,
                        })
                      }
                    />
                    {nameStatus && (
                      <div>
                        <p style={{ color: "red" }}>Please Enter your Name </p>
                      </div>
                    )}
                  </div>
                  <div class="form-group col-md-6">
                    <label for="inputEmail4">Email *</label>
                    <input
                      type="email"
                      class="form-control"
                      id="inputEmail4"
                      placeholder="Email"
                      value={userDetails.email}
                      onChange={(e) =>
                        setUserDetails({
                          ...userDetails,
                          email: e.target.value,
                        })
                      }
                    />
                      {emailStatus && (
                      <div>
                        <p style={{ color: "red" }}>Please Enter your Email </p>
                      </div>
                    )}
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group col-md-3">
                    <label for="inputEmail4">Age *</label>
                    <input
                      type="number"
                      class="form-control"
                      id="inputAge"
                      placeholder="Enter your age"
                      value={userDetails.age}
                      onChange={(e) =>
                        setUserDetails({
                          ...userDetails,
                          age: e.target.value,
                        })
                      }
                    />
                     {ageStatus && (
                      <div>
                        <p style={{ color: "red" }}>Please Enter your Age </p>
                      </div>
                    )}
                  </div>
                  <div class="form-group col-md-3">
                    <label for="inputEmail4">Date of Birth *</label>
                    <input
                      type="date"
                      class="form-control"
                      id="inputDate"
                      placeholder="Enter Your DOB"
                      value={userDetails.date}
                      onChange={(e) =>
                        setUserDetails({
                          ...userDetails,
                          date: e.target.value,
                        })
                      }
                    />
                     {dobStatus && (
                      <div>
                        <p style={{ color: "red" }}>Please Enter your DOB </p>
                      </div>
                    )}
                  </div>
                  <div class="form-group col-md-6">
                    <label for="inputEmail4 ">Gender</label>
                    
                    <div class="form-check form-check-inline  ml-3 mt-4">
                      <input
                        class="form-check-input"
                        type="radio"
                        name="inlineRadioOptions"
                        id="inlineRadio1"
                        // value="option1"
                        checked={userDetails.gender === "male"}
                        onClick={() => {
                          setUserDetails({
                            ...userDetails,
                            gender: "male",
                          });
                        }}
                      />
                      <label class="form-check-label" for="inlineRadio1">
                        Male
                      </label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input
                        class="form-check-input"
                        type="radio"
                        name="inlineRadioOptions"
                        id="inlineRadio2"
                        checked={userDetails.gender === "female"}
                        onClick={() => {
                          setUserDetails({
                            ...userDetails,
                            gender: "female",
                          });
                        }}
                      />
                      <label class="form-check-label" for="inlineRadio2">
                        Female
                      </label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input
                        class="form-check-input"
                        type="radio"
                        name="inlineRadioOptions"
                        id="inlineRadio2"
                        checked={userDetails.gender === "other"}
                        onClick={() => {
                          setUserDetails({
                            ...userDetails,
                            gender: "other",
                          });
                        }}
                      />
                      <label class="form-check-label" for="inlineRadio2">
                        Other
                      </label>
                    </div>
                    {genderStatus && (
                      <div className="mt-3">
                        <p style={{ color: "red" }}>Please Enter your Gender </p>
                      </div>
                    )}
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputAddress">Address *</label>
                  <input
                    type="text"
                    class="form-control"
                    id="inputAddress"
                    placeholder="1234 Main St"
                    value={userDetails.address}
                    onChange={(e) =>
                      setUserDetails({
                        ...userDetails,
                        address: e.target.value,
                      })
                    }
                  />
                   {addressStatus && (
                      <div>
                        <p style={{ color: "red" }}>Please Enter your Adddess </p>
                      </div>
                    )}
                </div>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="inputCity">City *</label>
                    <input
                      type="text"
                      class="form-control"
                      id="inputCity"
                      value={userDetails.city}
                      onChange={(e) =>
                        setUserDetails({
                          ...userDetails,
                          city: e.target.value,
                        })
                      }
                    />
                     {cityStatus && (
                      <div>
                        <p style={{ color: "red" }}>Please Enter your City </p>
                      </div>
                    )}
                  </div>
                  <div class="form-group col-md-4">
                    <label for="inputState">Country *</label>
                    <Select
                      options={options}
                      value={userDetails.country}
                      onChange={changeHandler}
                    />
                    {countryStatus && (
                      <div>
                        <p style={{ color: "red" }}>
                          Please select your Country Name 
                        </p>
                      </div>
                    )}
                  </div>
                  <div class="form-group col-md-2">
                    <label for="inputZip">Zip *</label>
                    <input
                      type="text"
                      class="form-control"
                      id="inputZip"
                      value={userDetails.zip}
                      onChange={(e) =>
                        setUserDetails({
                          ...userDetails,
                          zip: e.target.value,
                        })
                      }
                    />
                     {zipStatus && (
                      <div>
                        <p style={{ color: "red" }}>Please Enter your Zip code </p>
                      </div>
                    )}
                  </div>
                </div>
                <div class="form-group">
                  <div class="form-check">
                    <input
                      class="form-check-input"
                      type="checkbox"
                      id="gridCheck"
                      checked={isChecked}
                      onChange={() => setIsChecked(!isChecked)}
                    />
                    <label
                      class="form-check-label"
                      for="gridCheck"
                      style={!isChecked ? { color: "red" } : { color: "black" }}
                    >
                      Check me out *
                    </label>
                  </div>
                </div>
              </form>
            </div>
            <div class="card-footer text-right">
              <button
                type="submit"
                class="btn btn-primary mr-2"
                onClick={handleClear}
              >
                Clear
              </button>
              {editStatus ? (
                <button
                  type="submit"
                  class="btn btn-primary"
                  onClick={onUpdate}
                >
                  Update
                </button>
              ) : (
                <button
                  type="submit"
                  class="btn btn-primary"
                  onClick={onSubmit}
                >
                  Submit
                </button>
              )}
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default FormData;
