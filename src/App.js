import React from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import PersonalDetails from './component/PersonalDetails'
import OtpComponent from './OtpComponent'

function App() {
  return (
    <div>
         <Router>
            <Routes>
              <Route exact path="/" element={<OtpComponent/>}/>
                <Route exact path="/details" element={<PersonalDetails/>}/>
             </Routes>
        </Router>

    </div>
  )
}

export default App