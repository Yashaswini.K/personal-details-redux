import { CREATE, EDIT, DELETE, UPDATE, SELECT } from "./ActionType";

export const createRecord = (userDetails) => {
  return {
    type: CREATE,
    payload: userDetails,
  };
};

export const editRecord = (userDetails, id) => {
  return {
    type: EDIT,
    payload: userDetails,
    id: id,
  };
};

export const deleteRecord = (userDetails) => {
  return {
    type: DELETE,
    payload: userDetails,
  };
};

export const selectRecord = (userDetails) => {
  return {
    type: SELECT,
    payload: userDetails,
  };
};

export const updateRecord = (userDetails, id) => {
  return {
    type: UPDATE,
    payload: userDetails,
    id: id,
  };
};
