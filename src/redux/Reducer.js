import { CREATE, EDIT, DELETE, UPDATE, SELECT } from "./ActionType";

const initialState = {
  details: [],
};

function crudReducer(state = initialState, action) {
  debugger;
  switch (action.type) {
    case CREATE: {
      state.details.push(action.payload);
      state.details.map((val, index) => {
        val.isSelected = false;
      });
      return state;
    }

    case UPDATE: {
      debugger;
      state.details.splice(action.id, 1, action.payload);
      state.details.map((val, index) => {
        val.isSelected = false;
      });
      return state;
    }
    case EDIT: {
      debugger;
      state.details[action.id].isSelected = state.details[action.id].isSelected
        ? false
        : true;
      debugger;
      return state;
    }
    case DELETE: {
      debugger;
      let obj = {};
      obj.details = [...action.payload];
      return obj;
    }

    case SELECT: {
      debugger;
      let obj = {};
      obj.details = [...action.payload];
      return obj;
    }
    default:
      return state;
  }
}

export default crudReducer;
