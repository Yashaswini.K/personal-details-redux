import logo from "./logo.svg";
import "./App.css";
import { useState } from "react";
import firebase from "./firebase";
import { useNavigate } from "react-router-dom";

import {
  getAuth,
  signInWithPhoneNumber,
  RecaptchaVerifier,
} from "firebase/auth";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();
function OtpComponent() {
  const navigate = useNavigate();

  const [mobile, setMobile] = useState("");
  const [otp, setOTP] = useState("");
  const [errmobile, setErrorMobile] = useState(false);
  const [errotp, setOtpError] = useState(false);

  const configrereCaptcha = () => {
    const auth = getAuth();
    window.recaptchaVerifier = new RecaptchaVerifier(
      "sign-in-button",
      {
        size: "invisible",
        callback: (response) => {
          // reCAPTCHA solved, allow signInWithPhoneNumber.
          onSignInSubmit();
          toast.success("Recaptcha Verified", {
            position: toast.POSITION.BOTTOM_CENTER,
            autoClose: 1000,
          });
          // alert("recaptcha Verified");
        },
        defaultCountry: "IN",
      },
      auth
    );
  };

  const onSignInSubmit = (e) => {
    e.preventDefault();
    let isSubmit = false;
    if (mobile === undefined || mobile === "") {
      setErrorMobile(true);
      isSubmit = true;
    } else {
      setErrorMobile(false);
      // isSubmit = false;
    }
    if(isSubmit === false){
    
    configrereCaptcha();
    const phoneNumber = "+91" + mobile;
    debugger;

    const appVerifier = window.recaptchaVerifier;
    debugger;
    const auth = getAuth();
    debugger;
    signInWithPhoneNumber(auth, phoneNumber, appVerifier)
      .then((confirmationResult) => {
        // SMS sent. Prompt user to type the code from the message, then sign the
        // user in with confirmationResult.confirm(code).
        window.confirmationResult = confirmationResult;
        // alert("OTP has been sent");
        toast.success("OTP has been Sent", {
          position: toast.POSITION.BOTTOM_CENTER,
          autoClose: 1000,
        });
      })
      .catch((error) => {
        // Error; SMS not sent
        console.log(error);
        debugger;
        // ...
        toast.error("SMS not Sent", {
          position: toast.POSITION.BOTTOM_CENTER,
          autoClose: 1000,
        });
        // alert("SMS not sent");
      });
    }
  };

  const onSubmitOTP = (e) => {
    e.preventDefault();
    let isSubmit = false;
    if (otp === undefined || otp === "") {
      setOtpError(true);
      isSubmit = true;
    } else {
      setOtpError(false);
      // isSubmit = false;
    }
    if(isSubmit === false){
    const code = otp;
    debugger;
    window.confirmationResult
      .confirm(code)
      .then((result) => {
        // User signed in successfully.
        const user = result.user;
        // ...
        debugger;
        console.log(JSON.stringify(user));
        toast.success("User is Verified", {
          position: toast.POSITION.BOTTOM_CENTER,
          autoClose: 1000,
        });
        // alert("User is Verified");
        navigate("/details");
      })
      .catch((error) => {
        // User couldn't sign in (bad verification code?)
        // ...
        toast.error("Invalid Verification Code", {
          position: toast.POSITION.BOTTOM_CENTER,
          autoClose: 1000,
        });
        // alert("bad verification code");
      });
    }
  };

  return (
    <div className="App">
      <div
        class="card text-center"
        style={{ marginLeft: "500px", marginTop: "100px", width: "25rem" }}
      >
        <div class="card-body ">
          <h2 class="card-title">Log In</h2>
          <div class="mb-3 text-left">
            <form onSubmit={onSignInSubmit}>
              <div id="sign-in-button"></div>
              <label for="exampleInputPassword1" class="form-label">
                {" "}
                Mobile Number
              </label>
              <input
                class="form-control"
                type="number"
                name="mobile"
                placeholder="Enter Mobile Number"
                value={mobile}
                
                onChange={(e) => setMobile(e.target.value)}
              />
              {errmobile && (
                 <div>
                 <p style={{ color: "red" }}>Please enter mobile number </p>
               </div>
              )}
              <br></br>
              <button class="btn btn-primary btn-block" type="submit">
                Generate OTP
              </button>
            </form>
          </div>
          {/* <p class="card-text"></p> */}

          <div class="mb-3 text-left">
            <form onSubmit={onSubmitOTP}>
              <label for="exampleInputPassword1" class="form-label">
                {" "}
                OTP
              </label>
              <input
                class="form-control"
                type="number"
                name="otp"
                placeholder="Enter OTP"
                value={otp}
                onChange={(e) => setOTP(e.target.value)}
              />
              {errotp && (
                 <div>
                 <p style={{ color: "red" }}>Please enter otp </p>
               </div>
              )}
              <br></br>
              <button class="btn btn-success btn-block" type="submit">
                <i class="fas fa-sign-in-alt"></i> Sign in
              </button>
            </form>
          </div>
        </div>
      </div>
     
    </div>
  );
}

export default OtpComponent;
