import '../App.css'
import { useSelector, useDispatch } from "react-redux";
import FormData from "./FormData";
import TableList from "./TableList";
import { Link } from "react-router-dom";


import React, { useState, useEffect } from "react";
function PersonalDetails() {
  // Added
  const state = useSelector((state) => state.userDetails);
  const [isChecked, setIsChecked] = useState(false);

  const [modalStatus, setModalStatus] = useState(false);
  const [userDetails, setUserDetails] = useState({
    isSelected: false,
    name: "",
    email: "",
    age: "",
    date: "",
    gender: "male",
    address: "",
    city: "",
    country: "",
    zip: "",
  });
  const [checked, setChecked] = useState(false);
  const [newId, setID] = useState([]);
  const [editStatus, setEditStatus] = useState(false);

  // useEffect(() => {
  //   debugger;
  //   console.log(state, "state");
  // }, [state]);

  return (
    <div className="App">
         {/* nav start */}
      <nav
        className="navbar navbar-expand-lg navbar-dark bg-primary"
      
      >
        <Link className="navbar-brand" to="/details">
         <h3> Personal_Details</h3>
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNavAltMarkup"
          aria-controls="navbarNavAltMarkup"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div className="navbar-nav">
            {/* <Link className="nav-item nav-link active" to="/details">
              Dashboard <span className="sr-only">(current)</span>
            </Link> */}
           
            <Link
              className="nav-item nav-link active"
              to="/"
              style={{marginLeft:"1500%",border:"solid",borderRadius:"20px",background:'white',paddingLeft:"50%",paddingRight:"50%" }}
            >
             <b style={{color:"black"}}> Logout</b>
            </Link>
          </div>
        </div>
      </nav>
      {/* nav ends */}

      <FormData
        modalStatus={modalStatus}
        setModalStatus={setModalStatus}
        setUserDetails={setUserDetails}
        userDetails={userDetails}
        setID={setID}
        newId={newId}
        editStatus={editStatus}
        setEditStatus={setEditStatus}
        setIsChecked={setIsChecked}
        isChecked={isChecked}
        // toast={toast}
      />
      <TableList
        modalStatus={modalStatus}
        setModalStatus={setModalStatus}
        setUserDetails={setUserDetails}
        userDetails={userDetails}
        setID={setID}
        newId={newId}
        editStatus={editStatus}
        setEditStatus={setEditStatus}
        checked={checked}
        setChecked={setChecked}
        setIsChecked={setIsChecked}
        isChecked={isChecked}
        // toast={toast}
      />
    </div>
  );
}

export default PersonalDetails;
