import React, { useState, useEffect } from "react";

import { useSelector, useDispatch } from "react-redux";
import { deleteRecord, editRecord, selectRecord } from "../redux/Actions";
import { CREATE, EDIT } from "../redux/ActionType";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

function TableList({
  modalStatus,
  setModalStatus,
  setUserDetails,
  userDetails,
  setID,
  newId,
  editStatus,
  setEditStatus,
  checked,
  setChecked,
  setIsChecked,
  isChecked,
}) {
  const tableDetalis = useSelector((state) => state.details);
  const dispatch = useDispatch();

  const [countStatus, setCountStatus] = useState(0);
  const [dataCount, setDataCount] = useState(0);

  useEffect(() => {
    let arr = tableDetalis.filter((val) => {
      return val.isSelected === true;
    });
    if (tableDetalis.length > 0) {
      if (arr.length === tableDetalis.length) {
        setChecked(true);
      } else {
        setChecked(false);
      }
    }
  }, [JSON.stringify(tableDetalis)]);

  useEffect(() => {
    if (checked) {
      let idss = [];
      tableDetalis.map((val, index) => {
        val.isSelected = true;

        idss.push(index);
      });
      setID(idss);
    } else {
      let arr = tableDetalis.filter((val) => {
        return val.isSelected === true;
      });
      if (arr.length === tableDetalis.length) {
        tableDetalis.map((val, index) => {
          val.isSelected = false;
        });
        setID([]);
      }
    }

    dispatch(selectRecord(tableDetalis));
  }, [checked]);

  useEffect(() => {
    let count = 0;
    console.log(tableDetalis, "tableDetalis");
    tableDetalis.map((val, ind) => {
      if (val.isSelected) {
        count++;
      }
    });
    console.log(count);
    setDataCount(count)
    setCountStatus(count);
  }, [newId]);

  const handleCheck = (id) => {
    let ids = [...newId];
    let flag = "";
    for (let val of ids) {
      if (val === id) {
        flag = "present";
      }
    }

    if (flag === "present") {
      for (let ind in ids) {
        if (ids[ind] === id) {
          let a = JSON.parse(JSON.stringify(ids));
          a.splice(ind, 1);

          setID([...a]);
        }
      }
    } else {
      setID([...newId, id]);
    }

    // console.log(tableDetalis[id].payload.isSelected);
    let obj = tableDetalis[id];

    dispatch(editRecord(obj, id));
  };

  const editData = () => {
    if (newId.length > 0) {
      setUserDetails(tableDetalis[newId[0]]);
      setIsChecked(true);
      setModalStatus(true);
      setEditStatus(true);
    } else {
      setUserDetails({
        isSelected: false,
      });
    }
  };

  const deleteAll = () => {
    setChecked(false);
    console.log(tableDetalis);
    let copyData = [...tableDetalis];
    let filterData = copyData.filter((val, ind) => {
      return val.isSelected === false;
    });
    dispatch(deleteRecord(filterData));
    toast.success("Record deleted successfully", {
      autoClose: 2000,
      position: toast.POSITION.BOTTOM_CENTER,
    });
  };

  return (
    <div
      style={
        modalStatus
          ? {
              display: "none",
            }
          : {}
      }
    >
      <div class="row mt-3 mb-4">
        <div class="col-sm-8 text-left">
        </div>
        <div class="col-sm-4">
          <button
            type="button"
            class="btn btn-primary mr-3"
            style={{ display: "inline-block" }}
            onClick={() => {
              setModalStatus(true);
              setIsChecked(false);
              setUserDetails({
                isSelected: false,
                name: "",
                email: "",
                age: "",
                date: "",
                gender: "",
                address: "",
                city: "",
                country: "",
                zip: "",
              });
            }}
          >
            Add New
          </button>
          {console.log(countStatus, "countStatus")}
          {dataCount === 0  ? (
            <></>
          ):(
             (countStatus <= 1) ? (
              <>
                <i class="fas fa-pencil-alt mr-3" onClick={editData}></i>
                <i class="fas fa-trash-alt" onClick={deleteAll}></i>
              </>
            ) : (
              <>
                {/* <i class="fas fa-pencil-alt mr-3" onClick={editData}></i> */}
                <i class="fas fa-trash-alt" onClick={deleteAll}></i>
              </>
            )
          )}
        </div>
      </div>

      <table class="table table-striped">
        <thead>
          <tr>
            <th>
              {
                <input
                  type="checkbox"
                  checked={checked}
                  onChange={() => {
                    setChecked(!checked);
                  }}
                />
              }
            </th>
            <th>Sl. No</th>
            <th>User Name</th>
            <th>Email</th>
            <th>Age</th>
            <th>Date of Birth</th>
            <th>Gender</th>
            <th>Address</th>
            <th>City</th>
            <th>Country</th>
            <th>ZIP</th>
          </tr>
        </thead>
        <tbody>
          {tableDetalis.map((val, index) => {
            return (
              <tr>
                <td>
                  <input
                    type="checkbox"
                    onChange={() => handleCheck(index)}
                    checked={val.isSelected}
                  />
                </td>
                <td>{parseInt(index) + 1}</td>
                <td>{val.name}</td>
                <td>{val.age}</td>
                <td>{val.email}</td>
                <td>{val.date}</td>
                <td>{val.gender}</td>
                <td>{val.address}</td>
                <td>{val.city}</td>
                <td>{val.country.label}</td>
                <td>{val.zip}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default TableList;
